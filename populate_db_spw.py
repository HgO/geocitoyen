#!/usr/bin/python3
# coding: utf-8

import requests
import os
import sys

from time import sleep

from utils import normalize

def fetch_rues(cp):
    r = requests.get("http://geoservices.wallonie.be/geolocalisation/rest/getListeRuesOfAllCpsOfCommuneByCp/%s" % cp)
    
    data = r.json()
    if not data['rues']:
        return
    
    return r.content
    
def write(directory, commune, data):
    if not data:
        return
    
    with open(os.path.join(directory, normalize(commune) + '.json'), 'wb') as ofs:
        ofs.write(data)

def main():
    if len(sys.argv) < 2:
        print("usage:", os.path.basename(sys.argv[0]), "<directory> [postal_code]", file=sys.stderr)
        return
    
    directory = sys.argv[1]

    if len(sys.argv) > 2:
        cp = sys.argv[2]
        
        data = fetch_rues(cp)
        write(directory, cp, data)
        
    else:
        r = requests.get("http://geoservices.wallonie.be/geolocalisation/rest/getListeCommunes/")
        data = r.json()

        for commune in data['communes']:
            cp = commune['cps'][0]
            
            sleep(1)
            
            data = fetch_rues(cp)
            write(directory, commune['nom'], data)

if __name__ == '__main__':
    main()
