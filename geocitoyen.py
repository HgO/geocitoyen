#!/usr/bin/python3
# coding: utf-8

import spacy
import json

import plac

from geocoders import SPWGeocoder, NominatimGeocoder
from databases import Database, Adresse, Villes

class Parser:
    def __init__(self, nlp, communes, localites):
        self.nlp = nlp
        
        self.communes = communes
        self.localites = localites
    
    def parse(self, adresse, commune = None):
        doc = self.nlp(adresse.replace("’", "'").lower())
        
        labels = {}
    
        for entity in doc.ents:
            label = entity.label_.lower()
            
            if label not in labels:
                labels[label] = []
            
            labels[label].append(((entity.start_char, entity.end_char), entity.text))
        
        adresses = []
        
        for idx_rue, rue in labels.get('street', []):
            label_rue = idx_rue, 'street', rue
            
            if len(labels['street']) > 1 and doc[0].ent_type_.lower() in ('street', ''):
                prefix = doc[0].text.rstrip('s')
                    
                if not rue.startswith(prefix):
                    rue = prefix + ' ' + rue
            
            adresse = Adresse(rue)
            adresse.addLabel(rue, 'street', *idx_rue)
            
            if 'city' in labels:
                if len(labels['city']) == 2:
                    adresse.localite, adresse.commune = (v[1] for v in labels['city'])
                    
                    if self.localites.find(commune):
                        adresse.localite, adresse.commune = adresse.commune, adresse.localite
                else:
                    _, adresse.localite = labels['city'][0]
                
                for idx_ville, ville in labels['city']:
                    adresse.addLabel(ville, 'city', *idx_ville)
            else:
                adresse.commune = commune
                
            if 'cp' in labels:
                _, cp = labels['cp'][0]
                adresse.cp = int(cp)
                
                for idx_cp, cp in labels['cp']:
                    adresse.addLabel(cp, 'cp', *idx_cp)
            elif adresse.ville:
                adresse.cp = self.localites.code_postal(adresse.ville) or self.communes.code_postal(adresse.ville)
            
            for idx_num, num in labels.get('num', []):
                a = adresse.copy()
                a.num = num
                
                a.addLabel(num, 'num', *idx_num)
                
                adresses.append(a)
                
            if 'num' not in labels:
                adresses.append(adresse)
                
        return adresses
    
def extrapolate(adresse, *databases):
    adresse_db = None
    for db in databases:
        adresse_db = db.find(adresse)
    
        if adresse_db is not None: break
    
    return adresse_db
    
def main(
    adresses:   ("Nom du fichier contenant les adresses à géolocaliser. Par défaut, lit les adresses sur l'entrée standard (stdin)", 'option', 'i', str, None, 'adresses.txt'),
    model:      ("Nom ou chemin du modèle SpaCy", "positional"),
    commune:    ("Commune par défaut des différentes adresses.", 'positional'),
    communes:   ("Fichier json contenant la liste des communes (format: [ { 'ma_belcommune': { 'nom': \"Ma Bel'Commune\", 'cps': [ code_postaux ] } ])", 'option', 'c') = 'data/communes.json',
    localites:  ("Fichier json contenant la liste des localités (format: [ { 'mon_beau_village': { 'nom': \"Mon Beau Village\", 'cps': [ code_postaux ], 'commune': \"Ma Belle Commune\" } ])", 'option', 'l') = 'data/localites.json',
    *databases: ("Liste des bases de données des adresses. Chaque base de données doit être de la forme `type:chemin/vers/la/bdd`, où `type` peut être icar, spw, ou coord. L'ordre détermine la priorité de chacune des bases de données.", 'positional', None, str, [ 'icar:.+' ])):
    
    localites = Villes(localites)
    communes  = Villes(communes)
    
    databases = list(databases)
    for i, db in enumerate(databases):
        name, path = db.split(':', maxsplit=1)
        
        databases[i] = Database.create(name, path, communes, localites)
    
    if not adresses or adresses == '-':
        adresses = '/dev/stdin'
        
    with open(adresses) as ifs:
        dataset = [ line.strip() for line in ifs ]
    
    nlp = spacy.load(model)

    parser = Parser(nlp, communes, localites)
    
    geocoder_spw = SPWGeocoder()
    geocoder_nominatim = NominatimGeocoder()
    
    for text in dataset:
        print(text)
        for adresse in parser.parse(text, commune):
            result = extrapolate(adresse, *databases)
            
            if not result:
                result = adresse
                position = geocoder_nominatim.geocode(adresse)
            else:
                position = result.position
                
                if repr(result.source) == 'SPW':
                    position = geocoder_spw.geocode(result)
                
                if not position:
                    position = geocoder_nominatim.geocode(result)
                    
                if not position:
                    result.rue = adresse.rue
                    position = geocoder_nominatim.geocode(result)
            
            result.position = position
            
            print('->', result)

if __name__ == '__main__':
    plac.call(main)
