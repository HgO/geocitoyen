#!/usr/bin/python3
# coding: utf-8

import plac
import json

import random

import os
import re
import sys

from datetime import datetime

from utils import clean, normalize
from databases import Database, Villes

labels = [ 'street', 'city', 'cp', 'num' ]

masculins = re.compile(r"^(fond|parvis|jardin|clos|chemin|sent|hameau|pr[ée]|square|vieux|boulevard)")
determinants = re.compile(r"^(les?|des?|du|la)\b")
regex_labels = re.compile(r'<({labels})>(.+?)</\1>'.format(labels = '|'.join(labels)))

def make_tag(text, label):
    return '<{label}>{}</{label}>'.format(text, label = label)

def generate_range_num(num_start, prefix, suffix):
    num_end = random.randint(num_start+1, num_start+15)
    
    between = random.choice(( 'et', '-', 'à', 'au' )) 
    
    if between == '-':
        prefix = ''
    else:
        if between == 'au':
            has_from = bool(random.getrandbits(1))
            
            if has_from:
                prefix_from = 'du'
                
                if prefix:
                    prefix = prefix_from + ' ' + prefix
                else:
                    prefix = prefix_from
            
        elif len(prefix) > 3:
            prefix += 's'
        
        between = ' ' + between + ' '
    
    return prefix, between, make_tag(str(num_start) + suffix, 'num') + between + make_tag(num_end, 'num')

def generate_list_num(num, range_num, prefix):
    if prefix:
        if range_num > 1:
            if prefix[-1] not in '°xs':
                prefix += 's'
        else:
            has_approx = bool(random.getrandbits(1))
        
            if has_approx:
                prefix = random.choice((
                    'en face du',
                    'à hauteur du',
                    'à partir du',
                    'près du',
                    'à côté du',
                    'après le',
                    'avant le',
                    'le long du',
                    'devant le'
                )) + ' ' + prefix
    
    nums = [ num ]
    for i in range(1, range_num):
        nums.append(random.randint(nums[-1]+1, nums[-1]+5))
    
    num = ''
    for i, n in enumerate(nums):
        if i != 0:
            num += ', ' if i+1 < len(nums) else ' et '
        
        has_suffix = random.uniform(0, 1) < .1
        suffix = random.choice(( 'a', 'b', ' bis', ' ter', '/a' )) if has_suffix else ''
            
        num += make_tag(str(n) + suffix, 'num')
        
    return prefix, num

def generate_num(rue):
    num = random.randint(1, 250)
    
    is_before = bool(random.getrandbits(1))
    
    has_prefix = bool(random.getrandbits(1))
    has_suffix = bool(random.getrandbits(1))
    
    prefix = random.choice(( 'n°', 'numéro', 'numero' )) if has_prefix else ''
    suffix = random.choice(( 'a', 'b', ' bis', ' ter', '/a' )) if has_suffix else ''
    
    is_range = random.uniform(0,1) < .1
    is_list  = random.uniform(0,1) < .1
    
    has_parentheses = not is_before and (is_list or is_range) and random.uniform(0,1) < .1
    
    if is_range:
        prefix, between, num = generate_range_num(num, prefix, suffix)
    else:
        range_num = random.randint(2, 7) if is_list else 1
        prefix, num = generate_list_num(num, range_num, prefix)
    
    if prefix:
        num = prefix + ' ' + num
    
    if has_parentheses and not (is_range and between == '-'):
        num = '(%s)' % num
    
    if is_before:
        sep = random.choice(( '<det>', ', ', ' ' ))
            
        if sep == '<det>':
            if normalize(rue[0]) in 'aeiou':
                sep = " de l'"
            elif masculins.match(rue):
                sep = ' du '
            elif re.match(determinants, rue):
                sep = random.choice([ ' ', ', ' ])
            elif rue[:2] in ("l'", "d'"):
                sep = ' de '
            else:
                sep = ' de la '
        
        return num + sep + make_tag(rue, 'street')
    
    if not has_parentheses:
        sep = random.choice([ ' ', ', ' ])
    else:
        sep = ' '
        
    return make_tag(rue, 'street') + sep + num

def generate_city(rue, adresse):
    has_cp = bool(random.getrandbits(1))
    has_commune = bool(random.getrandbits(1))
        
    if has_commune and adresse.localite and adresse.localite != adresse.commune:
        ville = make_tag(adresse.commune, 'city') + ' (' + make_tag(adresse.localite, 'city') + ')'
    else:
        ville = make_tag(adresse.ville, 'city')
        
    if has_cp:
        cp = make_tag(adresse.cp, 'cp')
            
        ville = cp + ' ' + ville
        
    is_before = bool(random.getrandbits(1))
        
    if is_before:
        return ville + ', ' + rue
    
    sep = random.choice(( ', ', ' ' ))
    return rue + sep + ville

def generate(adresse, localites, communes):
    rue = adresse.rue.lower()
    
    if localites.find(rue) or communes.find(rue):
        return make_tag(rue, 'city')
    
    if len(rue) < 5 and rue.isdigit():
        nationale = random.choice(['rn', 'n'])
        rue = nationale + rue
        
        return make_tag(rue, 'street')
    
    has_city = bool(random.getrandbits(1))
    has_number = bool(random.getrandbits(1))
    
    if has_number:
        result = generate_num(rue)
    else:
        result = make_tag(rue, 'street')
    
    if has_city:
        result = generate_city(result, adresse)
    
    return result.lower()

def annotate(tagged_adresse, index = 0):
    annotations = []
    
    shift = 0
    for match in regex_labels.finditer(tagged_adresse):
        label = match.group(1).upper()
        
        total_start, total_end = match.span()
        start, end = match.span(2)
        
        text = match.group(2)
        
        delta_start, delta_end = start-total_start, total_end-end
        
        tagged_adresse = tagged_adresse[:total_start-shift] + text + tagged_adresse[total_end-shift:]
        
        start, end = start-delta_start-shift + index, end-delta_start-shift + index
        shift += delta_start + delta_end
    
        annotations.append((start, end, label, text))
        
    return tagged_adresse, annotations

def main(
    output : ("Dossier dans lequel générer les adresses annotées", "positional"),
    localites : ("Fichier json contenant la liste des localités", "option", "l") = 'data/localites.json',
    communes : ("Fichier json contenant la liste des communes", "option", "c") = 'data/communes.json',
    *databases : ("Liste des bases de données des adresses. Chaque base de données doit être de la forme `type:chemin/vers/la/bdd`, où `type` peut être icar, spw, ou coord. L'ordre détermine la priorité de chacune des bases de données.", "positional")):
    
    if len(databases) == 0:
        print('Vous devez indiquer au moins une base de données contenant des adresses postales.', file=sys.stderr)
        return
    
    localites = Villes(localites)
    communes  = Villes(communes)
    
    try:
        os.makedirs(output)
    except OSError:
        pass
    
    time_start = datetime.now()
    for database in databases:
        name, path = database.split(':', maxsplit=1)
        
        print("Création de la base de données", database)
        database = Database.create(name, path, communes, localites)
        
        print("Génération des adresses pour la commune :")
        for v, key_commune in enumerate(sorted(communes.villes), 1):
            print("- {:<30}({:d}/{:d})".format(communes.villes[key_commune]['nom'], v, len(communes.villes)), end=' ', flush=True)
            
            adresses = database.adresses(key_commune)
        
            if not adresses:
                print()
                continue
            
            tagged_adresses = []
            annotations = []
        
            index = 0
            for adresse in adresses.values():
                if isinstance(adresse, dict):
                    adresse = adresse[next(iter(adresse))]
                
                if adresse.localite:
                    adresse.localite = adresse.localite.split('/')[0].strip()
                adresse.commune = adresse.commune.split('/')[0].strip()
                
                tagged_adresse = generate(adresse, localites, communes)
                
                tagged_adresse, ann = annotate(tagged_adresse, index)
                annotations.extend(ann)
                
                index += len(tagged_adresse) + 2
                
                tagged_adresses.append(tagged_adresse)
            
            clusters = {}
            for adresse in adresses.values():
                if isinstance(adresse, dict):
                    adresse = adresse[next(iter(adresse))]
                
                rue = adresse.rue.lower()
                
                tokens = clean(rue, determinant=False).split()
                if len(tokens) < 2:
                    continue
                
                first_token = tokens[0]
                if len(first_token) < 3 or determinants.match(first_token):
                    continue
                
                if first_token not in clusters:
                    clusters[first_token] = []
                    
                clusters[first_token].append(rue[len(first_token)+1:])
            
            clusters = { token : rues for token, rues in clusters.items() if len(rues) > 1 }
            
            for token, rues in clusters.items():
                n_rues = random.randint(0, min(len(rues), 5))
                if n_rues > 1:
                    samples = random.sample(rues, n_rues)
                    
                    if token[-1] not in 'sx':
                        is_plural = random.uniform(0,1) > .1
                        
                        if is_plural:
                            token += 's'
                    
                    adresse = token
                    for i, rue in enumerate(samples):
                        if i != 0:
                            if i+1 < len(samples):
                                adresse += ',' 
                            elif ' et ' in adresse:
                                adresse += random.choice((', et', ','))
                            else:
                                adresse += random.choice((' et', ', et', ','))
                        
                        adresse += ' <street>%s</street>' % rue
                    
                    adresse, ann = annotate(adresse, index)
                    annotations.extend(ann)
                    
                    index += len(adresse) + 2
                    
                    tagged_adresses.append(adresse)
            
            path = os.path.join(output, key_commune) + '-' + str(database).lower()
            
            with open(path + '.txt', 'w') as ofs:
                ofs.write('\n\n'.join(tagged_adresses))
            
            with open(path + '.ann', 'w') as ofs:
                annotations.sort(key=lambda x: x[0])
                for i, (start, end, label, text) in enumerate(annotations):
                    ofs.write('T%d\t%s %d %d\t%s\n' % (i, label, start, end, text))

            del database.db[key_commune]
            
            elapsed_dt = (datetime.now() - time_start)
            elapsed_minutes = int(elapsed_dt.seconds / 60)
            elapsed_seconds = round(elapsed_dt.total_seconds() % (elapsed_minutes * 60)) if elapsed_minutes > 0 else elapsed_dt.total_seconds()
            
            remain_dt = elapsed_dt * (len(communes.villes) - v+1) / v
            remain_minutes = int(remain_dt.seconds / 60)
            remain_seconds = round(remain_dt.total_seconds() % (remain_minutes * 60)) if remain_minutes > 0 else remain_dt.total_seconds()
            
            print("temps écoulé: {:d}m {:.0f}s, temps restant: {:d}m {:.0f}s".format(elapsed_minutes, elapsed_seconds, remain_minutes, remain_seconds))
            
        
if __name__ == '__main__':
    plac.call(main)
