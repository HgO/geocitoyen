import json
import Levenshtein
import os
import csv

from copy import copy

from utils import normalize, clean, find_similar, FROM_PROJ, TO_PROJ
from pyproj import transform

class Adresse:
    def __init__(self, rue = None, num = None, localite = None, commune = None, cp = None, position = None, source = None):
        self.rue = rue
        self.num = num
        
        self.localite = localite
        self.commune = commune
        self.cp = cp
        
        self.labels = []
        
        self.position = position
        
        self.source = source
    
    def __getitem__(self, key):
        return getattr(self, key)
    
    def __setitem__(self, key, value):
        setattr(self, key, value)
    
    @property
    def ville(self):
        return self.localite if self.localite else self.commune
    
    def addLabel(self, text, label, start, end):
        self.labels.append((start, end, text, label.lower()))
    
    def copy(self):
        new = copy(self)
        new.labels = new.labels.copy()
        
        return new
    
    def __repr__(self):
        adresse = ''
        
        if self.rue:
            adresse += self.rue
            
            if self.num:
                adresse += ' ' + self.num
        
        if self.ville:
            if self.rue:
                adresse += ', '
            
            if self.cp:
                adresse += str(self.cp) + ' '
            
            adresse += self.ville
        
        if self.position:
            adresse += ' (%.6f, %.6f)' % self.position
        
        if self.source:
            adresse += ' ; source : ' + str(self.source)
        
        return adresse
        
class Database:
    keys = {}
    _instances = {}
    PERCENT_THRESHOLD = 85
    
    def __init__(self, path, communes, localites):
        self.db = {}
        self.path = path
        
        self.communes = communes
        self.localites = localites
    
    def create(name, *args):
        name = name.lower()
        
        if name not in Database._instances:
            databases = { 'icar' : ICARDatabase, 'spw' : SPWDatabase, 'coord' : CoordICARDatabase }
            
            Database._instances[name] = databases[name](*args)
            
        return Database._instances[name]
    
    def commune(self, ville):
        commune = self.communes.find(ville)
        
        if not commune:
            localite = self.localites.find(ville)
        
            if not localite:
                return
            
            commune = localite['commune']
        else:
            commune = commune['nom']
        
        return commune
    
    def key(value):
        value = value.lower()
        
        if value not in Database.keys:
            key = normalize(value)
            
            Database.keys[value] = key
            
        return Database.keys[value]
    
    def csv_reader(ifs):
        header = next(ifs)
            
        sep = ';' if ';' in header else ','
        
        return csv.reader(ifs, quotechar = '"', escapechar = '\\', delimiter = sep)
    
    def parse(self, filename, commune):
        raise NotImplementedError
    
    def find_key(self, rue, adresses):
        key_rue = clean(rue, determinant=False)
        cleaned_rue = clean(rue)
        
        percent = 100
        
        if key_rue in adresses:
            return key_rue, percent
        
        matches = [ key_rue_db for key_rue_db in adresses if cleaned_rue in clean(key_rue_db) ]
        if len(matches) > 0:
            return sorted(matches, key=len)[0], percent
        
        tokens = []
        for token in cleaned_rue.split():
            if token not in tokens:
                tokens.append(token)
                    
        initials = []
        for i, token in enumerate(tokens):
            if not (len(token) >= 2 and token[1] == '.' and i+1 < len(tokens)):
                continue
            
            next_token = tokens[i+1]
            initial = token[0]
            
            initials.append( (initial, next_token) )
            
        scored_rues = []
        for key_rue_db in adresses:
            cleaned_rue_db = clean(key_rue_db)
            
            tokens_db = []
            for token_db in cleaned_rue_db.split():
                if token_db not in tokens_db:
                    tokens_db.append(token_db)
                        
            score = 0
            for i, token_db in enumerate(tokens_db):
                for initial, next_token in initials:
                    if i+1 < len(tokens_db) and tokens_db[i+1] == next_token and token_db.startswith(initial):
                        score += 1
                    
            score += Levenshtein.ratio(cleaned_rue, cleaned_rue_db)
                    
            percent = round(score / (1 + len(initials)) * 100)
            if percent > Database.PERCENT_THRESHOLD:
                scored_rues.append((key_rue_db, percent))
        
        if len(scored_rues) > 0:
            scored_rues.sort(key=lambda x: (-x[-1], -len(x[0])))
                    
            return scored_rues[0]
        
        return
    
    def find(self, adresse):
        adresses = self.adresses(adresse.ville)
        
        if adresses is None:
            raise KeyError("Impossible d'obtenir la base de donnée pour", adresse.ville, "(%s)" % adresse.rue)
        
        match = self.find_key(adresse.rue, adresses)
        
        if match is None:
            return
        
        key_rue, _ = match
        
        adresse_db = adresses[key_rue]
        
        if not isinstance(adresse_db, Adresse):
            return adresse_db
        
        adresse_db = adresse_db.copy()
        
        if adresse.num and not adresse_db.num:
            adresse_db.num = adresse.num
            
        adresse_db.labels = adresse.labels
        
        return adresse_db
    
    def adresses(self, commune):
        commune = self.commune(commune)
        
        if not commune:
            return
        
        key_commune = Database.key(commune)
        
        if key_commune not in self.db:
            try:
                self.db[key_commune] = self.parse(self.path, key_commune)
            except FileNotFoundError:
                self.db[key_commune] = None
        
        return self.db[key_commune]

class SPWDatabase(Database):
    def __repr__(self):
        return 'SPW'
    
    def parse(self, path, key_commune):
        filename = os.path.join(path, key_commune + '.json')
        
        with open(filename) as ifs:
            data = json.load(ifs)
            
        adresses = {}
        for adresse in data['rues']:
            rue = adresse['nom'].lower()
            key_rue = clean(rue, determinant=False)
            
            localite = adresse['localites'][0].lower() if len(adresse['localites']) > 0 else None
            
            adresses[key_rue] = Adresse(
                rue,
                cp = adresse['cps'][0],
                localite = localite,
                commune = adresse['commune'].lower(),
                source = self
            )
        
        return adresses
    
class ICARDatabase(Database):
    def __repr__(self):
        return 'ICAR'
    
    def parse(self, filename, key_commune):
        adresses = {}
        
        with open(filename) as ifs:
           for line in Database.csv_reader(ifs):
                commune = line[8]
                
                if key_commune != Database.key(commune):
                    continue
                
                rue = line[3]
                key_rue = clean(rue, determinant=False)
                
                adresses[key_rue] = Adresse(
                    rue,
                    commune = commune,
                    cp = self.communes.code_postal(commune),
                    source = self
                )
        
        return adresses
    
class CoordICARDatabase(Database):
    def __repr__(self):
        return 'ICAR-COORD'
    
    def parse(self, filename, key_commune):
        adresses = {}
        
        with open(filename) as ifs:
            for line in Database.csv_reader(ifs):
                commune = line[16]
                
                if key_commune != Database.key(commune):
                    continue
                
                rue = line[10]
                key_rue = clean(rue, determinant=False)
                
                if key_rue not in adresses:
                    adresses[key_rue] = {}
                
                num = line[1]
                
                assert num not in adresses[key_rue]
                
                try:
                    cp = int(line[13])
                except ValueError:
                    cp = self.communes.code_postal(commune)
                
                localite = line[14]
                if not localite:
                    localite = None
                
                adresses[key_rue][num] = Adresse(
                    rue,
                    num = num.lower(),
                    commune = commune,
                    cp = cp,
                    localite = localite,
                    position = (float(line[-2]), float(line[-1])),
                    source = self
                )
                
        return adresses
    
    def find(self, adresse):
        adresses = super().find(adresse)
        
        if adresses is None:
            return
        
        if adresse.num is None:
            adresse_db = adresses[next(iter(adresses))].copy()
            adresse_db.num = None
            
            positions = (adresse.position for adresse in adresses.values())
            adresse_db.position = tuple(sum(xs) / len(adresses) for xs in zip(*positions))
        else:
            adresse_db = find_similar(adresse.num, adresses)
        
            if adresse_db is None:
                return
        
            adresse_db = adresse_db.copy()
        
        adresse_db.position = tuple(reversed(transform(FROM_PROJ, TO_PROJ, *adresse_db.position)))
        adresse_db.labels = adresse.labels
        
        return adresse_db
        
class Villes:
    def __init__(self, filename):
        self.villes = {}
        with open(filename) as ifs:
            for key, value in json.load(ifs).items():
                try:
                    self.villes[key] = value.lower() 
                except AttributeError:
                    self.villes[key] = value
        
        self.cps = {}
        for ville in self.villes.values():
            for cp in ville['cps']:
                self.cps[cp] = ville['nom']
                
        self.cache = {}
    
    def find(self, ville):
        ville = ville.lower()
        
        if ville not in self.cache:
            key_ville = normalize(ville)
        
            self.cache[ville] = find_similar(key_ville, self.villes)
        
        return self.cache[ville]
    
    def code_postal(self, ville):
        match = self.find(ville)
        
        if not match:
            return
        
        return match['cps'][0]
    
    def ville(self, cp):
        return self.cps[cp]
        
