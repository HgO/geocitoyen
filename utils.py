# coding: utf-8

import re
import string
from unidecode import unidecode

from pyproj import Proj

FROM_PROJ = Proj("+proj=lcc +lat_1=51.16666723333333 +lat_2=49.8333339 +lat_0=90 +lon_0=4.367486666666666 +x_0=150000.013 +y_0=5400088.438 +ellps=intl +towgs84=-99.059,53.322,-112.486,0.419,-0.830,1.885,-1", errcheck=True)
TO_PROJ   = Proj("+proj=longlat +datum=WGS84", errcheck=True)

extra_spaces = re.compile(r'\s+')
determinants = re.compile(r'\b(des?|les?|la|l|d|du)\b')
premier = re.compile(r'\bier\b')
nationale = re.compile('^(?:rn\s*)?([0-9]+)$')

def normalize(s):
    s = s.strip()
    
    valid_chars = '.-_() %s%s' % (string.ascii_letters, string.digits)
    replace_chars = '- '
    
    s = unidecode(s)
    s = ''.join(' ' if c in replace_chars else c for c in s if c in valid_chars)
    s = extra_spaces.sub(' ', s).lower().replace(' ', '_')
    
    return s

def clean(s, determinant=True):
    s = s.lower()
    s = unidecode(s.replace('°', ' ')).replace("-", " ").replace("'", " ").replace('/', ' ').replace('.', '. ')
    if determinant:
        s = determinants.sub('', s)
    s = extra_spaces.sub(' ', s).strip()
    
    s = premier.sub('1er', s)
    s = nationale.sub(r'n\1', s)
    
    return s

def find_similar(string, d):
    if string not in d:
        matches = [ key for key in d if string in key ]
    
        if not matches:
            return
    
        matches.sort(key=len)
    
        string = matches[0]
        
    return d[string]
