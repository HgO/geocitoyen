# Introduction

Ce logiciel permet de convertir une adresse située en Wallonie en coordonnées GPS.

Le logiciel est capable d'extrapoler les adresses à partir des bases de données de l'ICAR et du SPW (voir [plus bas](#Bases de données)). Il est possible d'indiquer quelle base de données est prioritaire. Ceci est particulièrement utile quand une base de données est globalement plus précise ou complète que les autres.

Ensuite, les coordonnées des adresses sont récupérées soit à partir des [services de géolocalisation du SPW](http://geoservices.wallonie.be/geolocalisation/), soit à partir de [Nominatim](http://nominatim.openstreetmap.org/).  
Dans certains cas, les coordonnées sont déjà présentes dans la base de données et aucun service de géolocalisation supplémentaire n'est alors requis.

# Pré-requis

Ce logiciel nécessite les librairies suivantes :
* spacy
* unidecode
* plac
* Levenshtein
* pyproj

# Utilisation

Pour extrapoler et géolocaliser une liste d'adresses :
```
cat adresses.txt | ./geocitoyen.py model commune bdd1 [bdd2 ...]
```

* `adresses.txt` est un fichier contenant la liste des adresses à géolocaliser.
* `model` est un modèle spaCy capable d'identifier les éléments d'une adresse, à savoir la ville (city), le code postal (cp), la rue (street) et le numéro (num). 
* `commune` est la commune par défaut lorsque l'adresse ne contient aucune ville.
* `bdd1, bdd2, ...` sont les bases de données contenant la liste des adresses connues. Chaque base de données doit être de la forme `type:chemin/vers/la/bdd1`. Les différents types possibles sont `spw`, `icar` ou `coord`.

Attention : Le logiciel consomme beaucoup de mémoire (~500Mo) dû au modèle spaCy.

Pour obtenir les coordonnées d'une adresse (sans extrapolation) :
```
./geocoders.py rue [numéro] -c code_postal -v ville -g geocoder
```

* `geocoder` est le nom du service de géolocalisation, et peut valoir soit `nominatim`, soit `spw`

## Exemple

Le logiciel peut être utilisé comme module python :
```python
import spacy
import geocitoyen as geo

localites = geo.Villes('data/localites.json')
communes  = geo.Villes('data/communes.json')

db_spw  = geo.Database.create('spw', path_db_spw, communes, localites)
db_icar = geo.Database.create('icar', path_db_icar, communes, localites)

nlp = spacy.load(model)

parser = geo.Parser(nlp, communes, localites)

geocoder_spw = geo.SPWGeocoder()
geocoder_nominatim = geo.NominatimGeocoder()

dataset = [ "Rue de Fer, du n° 63 au n° 99 à Namur", "Chaussée de Charleroi, 83 bis" ]
commune = "Namur"

for text in dataset:
    print(text)
    # `commune` est le nom de la commune par défaut
    for adresse in parser.parse(text, commune):
        print(adresse)
        # Mettre la base de données prioritaire en premier
        result = geo.extrapolate(adresse, db_spw, db_icar)
        
        if not result:
            position = geocoder_nominatim.geocode(adresse)
            result = adresse
        else:
            if result.source == db_spw:
                position = geocoder_spw.geocode(result)
            else:
                position = geocoder_nominatim.geocode(result)
        
        print(result, '(%.6f, %.6f)' % position)
```

Par exemple, l'adresse `Rue de Fer, du n° 63 au n° 99 à Namur` donnera les coordonnées :
* Rue de Fer 63, 5000 Namur ([50.465896, 4.865299](https://www.openstreetmap.org/?mlat=50.46590&mlon=4.86530#map=19/50.46590/4.86530))
* Rue de Fer 99, 5000 Namur ([50.467004, 4.865400](https://www.openstreetmap.org/?mlat=50.46700&mlon=4.86540#map=19/50.46700/4.86540))

## Bases de données

### ICAR

La base de données de l'ICAR est [téléchargeable ici](http://geoportail.wallonie.be/catalogue/cbd93a53-87d1-4f1a-8a7a-751bfc982325.html) au format CSV. Vous pouvez la charger via `Database.create('icar', path, communes, localites)`.

Il [existe également](http://geoportail.wallonie.be/catalogue/2998bccd-dae4-49fb-b6a5-867e6c37680f.html) une version plus complète de cette base de données avec les coordonnées GPS de chacune des adresses postales wallonnes (1.3 millions d'adresses). Vous pouvez faire la demande auprès du SPW pour avoir accès à cette base de données. Pour ce faire, créez-vous un compte sur le géoportail de la Wallonie, puis suivez les instructions [ici](http://geoportail.wallonie.be/telecharger). **Attention :** Ces données sont soumises à des **[conditions d'utilisation](http://geoportail.wallonie.be/files/documents/ConditionsSPW/DataSPW-CGU.pdf)** que je vous recommande vivement de lire ! Vous ne pouvez en effet pas transmettre ces données à d'autres personnes, par exemple. Par contre, si vous faites la demande auprès du SPW, ils ne pourront en principe pas vous la refuser.

Vous pouvez charger cette base de données en spécifiant le type `coord`. 

### SPW

La base de données du SPW peut être téléchargée [à partir de leurs services de géolocalisation](http://geoservices.wallonie.be/geolocalisation/doc/ws/index.xhtml). Cette base de données est plus précise que celle de l'ICAR, mais est malheureusement moins à jour. Pour la charger, indiquez le type `spw` lors de la création de la base de données.

Pour remplir la base de données du SPW, il suffit de lancer le script suivant (cela prendra un certain temps, ceci afin de ne pas surcharger les serveurs du SPW) :
```
./populate_db_spw.py <dossier> [code_postal]
```

## Modèle spaCy

Le modèle [spaCy](http://spacy.io/) permettant d'identifier automatiquement les différents éléments d'une adresse a été entraîné sur une centaine de milliers d'adresses' générées automatiquement à partir des données de l'ICAR et du SPW. Il a également été entraîné sur des centaines d'adresses extraites de documents administratifs. 

Ce modèle est disponible dans le dossier `model/model-final` et sera amélioré au fil du temps. Si vous trouvez une adresse mal identifiée, merci d'[ouvrir un ticket](https://framagit.org/HgO/geocitoyen/issues) pour qu'on en discute.

Si vous désirez entraîner le modèle avec vos propres données, vous pouvez générer un ensemble d'adresses fictives à partir des différentes bases de données à votre disposition. Pour ce faire, vous exécutez le script suivant :
```python
./generate.py output bdd1 [bdd2 ...]
```
Le script parcourt la liste des rues de chaque commune pour créer des adresses fictives, avec ou sans numéro(s), ville, etc. Chaque partie de l'adresse étant bien entendu annotée pour permettre l'apprentissage. Les fichiers générés le sont au [format Standoff](http://brat.nlplab.org/standoff.html). La conversion vers un [format supporté par spaCy](https://spacy.io/api/annotation#json-input) est relativement triviale.
