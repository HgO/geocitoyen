#!/usr/bin/python3
# coding: utf-8

from databases import Adresse
from utils import FROM_PROJ, TO_PROJ
from pyproj import transform

from datetime import datetime
from time import sleep

import requests
import sys

import plac

class Geocoder:
    def __init__(self, throttle = 1):
        self.cache = {}
        self.time = datetime.fromtimestamp(0)
        
        self.throttle = throttle
    
    def geocode(self, adresse):
        key_adresse = self.make_key(adresse)
        
        if key_adresse not in self.cache:
            dt = datetime.today() - self.time
            sleep(max(self.throttle - dt.total_seconds(), 0))
            
            data = self.fetch(adresse)
            
            self.time = datetime.today()
            
            if not data:
                return
            
            self.cache[key_adresse] = self.parse(data)
        
        return self.cache[key_adresse]
    
    def make_key(self, adresse):
        return NotImplementedError
    
    def fetch(self, adresse):
        raise NotImplementedError
    
    def parse(self, adresse):
        raise NotImplementedError

class SPWGeocoder(Geocoder):
    def make_key(self, adresse):
        if adresse.num:
            return (adresse.rue, adresse.num, adresse.cp)
        
        return (adresse.rue, adresse.cp)
    
    def fetch(self, adresse):
        if adresse.num:
            url = "http://geoservices.wallonie.be/geolocalisation/rest/getPositionByCpRueAndNumero/{adresse.cp}/{adresse.rue}/{adresse.num}"
        else:
            url = "http://geoservices.wallonie.be/geolocalisation/rest/getPositionByCpAndRue/{adresse.cp}/{adresse.rue}"
        
        #print(url.format(**adresse))
        
        response = requests.get(url.format(adresse = adresse))
        
        if response.status_code != 200:
            return
        
        return response.json()
    
    def parse(self, data):
        if data['errorCode'] != 0:
            print('Erreur %d:' % data['errorCode'], data['errorMsg'], file=sys.stderr)
        
        x, y = data['x'], data['y']
        
        if x is None or y is None:
            return
        
        if str(data['rue']['cps'][0]) not in data['adresse']:
            return
        
        return tuple(reversed(transform(FROM_PROJ, TO_PROJ, x, y)))
    
class NominatimGeocoder(Geocoder):
    def make_key(self, adresse):
        if adresse.num:
            return (adresse.rue, adresse.num, adresse.ville)
        
        return (adresse.rue, adresse.ville)
    
    def fetch(self, adresse):
        rue = adresse.rue
        if adresse.num:
            rue += ' ' + adresse.num
        
        cp = adresse.cp if adresse.cp else ''
        ville = adresse.ville if adresse.ville else ''
        
        url = "https://nominatim.openstreetmap.org/search/?street={rue}&postalcode={cp}&city={ville}&countrycodes=be&limit=1&format=jsonv2"
        
        response = requests.get(url.format(rue = rue, cp = cp, ville = ville))
        
        if response.status_code != 200:
            return
        
        return response.json()
    
    def parse(self, data):
        if not data:
            return
        
        data = data[0]
        
        return float(data['lat']), float(data['lon'])
    
def main(
    rue: ("Nom de la rue", "positional", None, str),
    cp: ("Code postal", "option", 'c', int, None, 'code_postal'),
    ville: ("Ville", "option", 'v', str, None, 'ville'),
    num: ("Numéro de la rue", "positional", None, str) = None,
    geocoder: ("Geocoder à utiliser", "option", "g", str, [ 'nominatim', 'spw' ]) = 'spw'
    ):
    
    adresse = Adresse(rue, num = num, cp = cp, localite = ville)
    
    geocoder = SPWGeocoder() if geocoder == 'spw' else NominatimGeocoder()
    position = geocoder.geocode(adresse)
    
    if position:
        print(*position)

if __name__ == '__main__':
    plac.call(main)
